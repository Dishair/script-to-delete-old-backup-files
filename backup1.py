#!/usr/bin/env python3
# -*- coding: utf-8 -*- 

import re
import subprocess
from subprocess import run
import time
from datetime import datetime

root_folder = '/mnt/backups'

# Запрашиваем испольуземое место по папкам

def run_it():
    def get_using_space():
        using_space = subprocess.run(['du ' + root_folder + '/*'], stdout=subprocess.PIPE, shell=True, universal_newlines=True)
        return using_space.stdout





    # Получаем строку с свободным местом
    def get_free_space():
        free_space = subprocess.run(['df ' + root_folder], stdout=subprocess.PIPE, shell=True, universal_newlines=True)
        #print (free_space)
        match = re.search(r'\d+\s+\d+\s+\d+', free_space.stdout)
        if match:
            free_space = (match.group())
        free_space = re.sub(r'\d+\s+\d+\s+',  '', free_space)
        # print (free_space)
        return free_space

    # free_space = int(get_free_space())

    # print ("Free space is:",free_space)

    # Получаем список размера папок
    def find_biggest_size_folder():
        # all_folders = re.findall(r'\d+\t+..', get_using_space())
        all_folders = re.findall(r'\d+\t+\/mnt\/backups\/backup', get_using_space()) # choose only folders what created by backup script
        # print (all_folders)

        # Создаем список в числах
        all_folders_int = []

        # Добавляем данные в список в числах
        # for folder in all_folders:
        #   all_folders_int.append(int(str(folder)[:-2]))



        for folder in all_folders:
            folder=re.sub(r'\t+\/mnt\/backups\/backup', '', folder)
            #print (folder)
            all_folders_int.append(int(folder))


        # print (all_folders_int)

        # Поиск самой большой папки 
        biggest_size_folder = 0
        for folder in all_folders_int:
            if folder > biggest_size_folder:
                    biggest_size_folder = folder
        return biggest_size_folder



    # print ("Biggest size folder is:",(biggest_size_folder))

    # Получаем дату создания самой старой папки
    def oldest_folder():
        folder_names = re.split(r'\d+\s+\.', get_using_space())
        #print (folder_names)
        folder_names_new = []
        # Очищаем список от левых символов и оставляем только дату
        for folder in folder_names:
            match = re.search(r'\d+-\d+-\d+', folder)
            if  match:
                folder_names_new.append(match.group())
        #print (folder_names_new)
        folder_time = []
        for folder in folder_names_new:
            folder_time.append(datetime.strptime(folder, '%d-%m-%Y'))
        #print (folder_time)
        oldest_folder = min (folder_time)
        #print ( oldest_folder)
        return (oldest_folder)



    # Получаем пути до всех папок
    def all_paths():
        folder_paths = re.split(r'\d+\s+\.', get_using_space())
        folder_paths_new = []
        # Очищаем список от левых символов и оставляем только дату
        for folder in folder_paths:
            match = re.search(r'/\D+\d+-\d+-\d+', folder)
            if  match:
                folder_paths_new.append(match.group())
        return (folder_paths_new)




    # Получаем путь до самой старой папки
    def oldest_folder_path():
        for path in all_paths():
            match = (re.search(r'\d+-\d+-\d+', path))
            if (datetime.strptime(match.group(), '%d-%m-%Y')) == (oldest_folder()):
                oldest_path = path
        return (oldest_path)

    # print ('Oldest folder path:', oldest_folder_path())


        # Удаляем самую старую папку
    def delete_oldest_folder():
        delete_folder = subprocess.run(['rm -R ' + oldest_folder_path()], stdout=subprocess.PIPE, shell=True, universal_newlines=True)
        return (print (("Oldest folder deleted"), delete_folder.stdout))

    #print (delete_oldest_folder())




    while int(get_free_space()) <= ((find_biggest_size_folder())*1.2):
        try: 
            print ('free space is:', (int(get_free_space())))
            print ('Biggest folder is:',find_biggest_size_folder())
            print ('no free space')
            delete_oldest_folder()
        except UnboundLocalError: 
            print ("Error")
            break

    else:
        print ('free space is:', (int(get_free_space())))
        print ('Biggest folder is:',find_biggest_size_folder())
        print ('Start backup')
        subprocess.run(['/home/teqadmin/backup/backup.sh'], stdout=subprocess.PIPE, shell=True, universal_newlines=True)
        print ('End backup')

    return ()

run_it()
